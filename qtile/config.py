# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import os
import signal
import subprocess
import time

# qtile
from libqtile import bar, layout, widget, hook, backend, qtile
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
#from libqtile.utils import guess_terminal

# addons
from addons.interface import DBusService
from addons.wallpaper import Wallpapers


# -----------------------------------------------------------------------------
# log to journal
from libqtile.log_utils import logger
import systemd.journal

logger.handlers.clear()

handler = systemd.journal.JournalHandler(SYSLOG_IDENTIFIER="qtile")
formatter = logging.Formatter("%(name)s: %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)

# set loglevel
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)  # x11 backend is very verbose

# -----------------------------------------------------------------------------

@hook.subscribe.startup
def startup():
    logger.info("startup")

    if qtile.core.name == "wayland":
        logger.info("running using wlroots")

    if qtile.core.name == "x11":
        logger.info("running using x11")

        # start compositor
        global proc_picom
        proc_picom = subprocess.Popen(["picom", "--vsync", "--backend", "glx", "--no-fading-openclose"])


@hook.subscribe.shutdown
def shutdown():
    logger.info("shutdown")

    # TODO
    # send sigterm to all windows
    windows = [w for w in qtile.windows_map.values() if isinstance(w, backend.base.Window)]
    logger.info("windows: %s", windows)

    for w in windows:
        pid = w.get_pid()
        logger.info("send sigterm to %s", pid)
        os.kill(pid, signal.SIGTERM)
    time.sleep(3)


    if qtile.core.name == "x11":
        proc_picom.terminate()

@hook.subscribe.restart
def restart():
    logger.info("restarting")

@lazy.function
def reload_config(qtile):
    logger.info("reloading config")
    lazy.reload_config()


# -----------------------------------------------------------------------------
# dbus interface

@hook.subscribe.startup_once
def start_dbusservice():
    global dbusservice
    dbusservice = DBusService(logger, qtile)
    dbusservice.start()

@hook.subscribe.shutdown
def stop_dbusservice():
    dbusservice.stop()

@hook.subscribe.client_managed
def dbusservice_new_window(window):
    if not isinstance(window, backend.base.Window):
        return
    pid = window.get_pid()
    logger.info("new window %s", pid)
    dbusservice.new_window(pid)

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# wallpaper

# this runs after setgroup?!
@hook.subscribe.startup_once
def wp_startup():
    logger.info("startup wallpaper")
    global wallpapers
    wallpapers = Wallpapers(logger, qtile)

# this seems to be issued for each screen
# it is (always BUG?) issued twice, but qtile.current_screen is always the focused screen
@hook.subscribe.setgroup
def wp_setgroup():
    # this is called before startup hook
    try:
        wallpapers.update()
    except NameError:
        pass

@hook.subscribe.addgroup
def wp_addgroup(label):
    # this is called before startup hook
    try:
        wallpapers.add_group(label)
    except NameError:
        pass

@hook.subscribe.delgroup
def wp_delgroup(label):
    try:
        wallpapers.remove_group(label)
    except NameError:
        pass

@lazy.function
def wp_reroll(qtile):
    wallpapers.roll()

@lazy.function
def wp_switch(qtile):
    wallpapers.switch()

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# screenlock

@lazy.function
def lock_screen(qtile):
    # TODO check for wayland
    fp = wallpapers.get_lockscreen()
    proc_lock = subprocess.Popen(
        ["swaylock", "--inside-color", "0000003F", "--image", fp],
        start_new_session=True
    )

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# auto float dialogs (wlroots)

@hook.subscribe.client_new
def floating_dialogs(window):
    logger.info("new window %s type %s", window, type(window))
    if not isinstance(window, backend.base.Window):
        return

    transient = window.is_transient_for()
    logger.info("transient %s", transient)
    if transient:
        window.floating = True

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# kanshi screen manager

# ~/.config/kanshi/config

# TODO x11
# use autorandr?
# https://github.com/phillipberndt/autorandr

@hook.subscribe.startup_once
def start_kanshi():
    if not qtile.core.name == "wayland":
        return

    logger.info("starting kanshi")
    global kanshi
    kanshi = subprocess.Popen("kanshi 2>&1 | systemd-cat -t kanshi", start_new_session=True, shell=True)

@hook.subscribe.shutdown
def stop_kanshi():
    if not qtile.core.name == "wayland":
        return

    logger.info("stopping kanshi")
    kanshi.kill()

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# TODO needed?

@hook.subscribe.startup_once
def setup_portal():
    if not qtile.core.name == "wayland":
        return

    logger.info("setup portal")
    subprocess.Popen(["systemctl", "--user", "import-environment", "WAYLAND_DISPLAY"])
    #subprocess.Popen(["systemctl", "--user", "import-environment", "XDG_CURRENT_DESKTOP"])

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
@lazy.function
def new_group(qtile):
    logger.info("creating new group")
    currentgroup = qtile.current_group
    gname = str(len(qtile.groups) + 1)
    qtile.add_group(gname)
    i = qtile.groups.index(currentgroup) + 1
    qtile.groups.insert(i, qtile.groups.pop())
    group = qtile.groups_map[gname]
    qtile.current_screen.set_group(group)

# -----------------------------------------------------------------------------


mod = "mod4"
#terminal = guess_terminal()
terminal = "kitty"

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    #Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # TODO floating windows are never reached
    Key(["mod1"], "tab", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    # TODO test
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "m", lazy.layout.reset(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    #Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    #Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "r", reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    #Key([mod], "space", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "space", lazy.spawn("seamless"), desc="start launcher"),

    # set window floating
    Key([mod], "f", lazy.window.toggle_floating(), desc="(un)set window floating"),

    # TODO shortcut, new group
    Key([mod], "g", new_group, desc="create new group"),

    # TODO alt tab

    #Key([mod], "w", lazy.to_screen(1), desc="move focus to next screen"),
    #Key([mod], "s", lazy.to_screen(0), desc="move focus to prev screen"),
    Key([mod], "w", lazy.next_screen(), desc="move focus to next screen"),
    Key([mod], "s", lazy.prev_screen(), desc="move focus to prev screen"),
    Key([mod], "d", lazy.screen.next_group(skip_managed=True), desc="switch to next group"),
    Key([mod], "a", lazy.screen.prev_group(skip_managed=True), desc="switch to prev group"),

    # TODO move current window to group on other screen
    #Key([mod, "shift"], "w", lazy.layout.shuffle_left(), desc="Move window to the left"),

    Key([mod], "l", lock_screen, desc="lock screen"),

    Key([mod], "r", wp_reroll, desc="randomize wallpapers"),
    Key([mod, "shift"], "r", wp_switch, desc="switch wallpaper set"),

]



#groups = [Group(i) for i in "1234567890"]
groups = [Group(i) for i in "12"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )


# -----------------------------------------------------------------------------
# test layout

class Quad(layout.Columns):

    def __init__(self, **config):
        # TODO is this instanciated per group?
        super().__init__(**config)
        self._count = 0

    # TODO why is this not called?
    #def add_client(self, client, offset_to_current=0, client_position=None):

    def add(self, client):
        self._count += 1
        logger.info("Quad Layout: add count %s", self._count)
        return super().add(client)

    def remove(self, client):
        self._count -= 1
        logger.info("Quad Layout: rem count %s", self._count)
        return super().remove(client)

# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# auto generate new groups

maxwin = {"floating": 0, "monadtall": 3, "columns": 2, "quad": 4}

@hook.subscribe.client_new
def extend(window):
    #if not isinstance(window, libqtile.backend.wayland.xdgwindow.XdgWindow):
    if not isinstance(window, backend.base.Window):
        return

    currentgroup = qtile.current_group
    cur_layout = currentgroup.layout

    # TODO only count non floating window.floating()
    num = len(currentgroup.windows) + 1  # is the current window not (yet) in windows list?

    #num2 = 1
    #for window in currentgroup.windows:
    #    if not window.floating:
    #        num2 += 1

    #logger.info("check for new group, nr win %s %s", num, num2)

    # FIXME there seems to be a race condition
    # if this is too slow(?) then the last window is moved to the new group together with the new win?!

    nummax = maxwin.get(cur_layout.name, 0)

    if nummax and num > nummax:
        gname = currentgroup.name + "+"
        i = qtile.groups.index(currentgroup) + 1

        # new group
        qtile.add_group(gname)
        # reorder groups
        # https://github.com/qtile/qtile/pull/4035
        #qtile.add_group(gname, index=i)  # FIXME wait for >0.22.1
        qtile.groups.insert(i, qtile.groups.pop())

        group = qtile.groups_map[gname]

        # TODO does not work
        #group.persist = True

        # set layout
        #group.layout = "quad"
        group.layout = cur_layout.name

        # move new window to new group
        window.togroup(gname)

        # switch to group
        qtile.current_screen.set_group(group)

# -----------------------------------------------------------------------------


layouts = [
    #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    # setting colors seems to be broken
    #layout.Floating(border_focus="#fefefe", border_normal="#000000"),
    # border returns after window move
    layout.MonadTall(align=layout.MonadTall._right, border_width=0, margin=10),
    #Quad(align=layout.MonadTall._right, border_width=0, margin=10),
    layout.Columns(border_width=0, margin=10),
    Quad(fair=True, border_width=0, margin=10),
    layout.Max(margin=10),
    # TODO cycling over floating layout fucks up a group
    #layout.Floating(border_width=0),

    #layout.Slice(side="right", width=960, fallback=layout.VerticalTile()),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    #layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(),
                widget.CurrentLayout(),
                widget.CurrentScreen(),
                widget.Prompt(),
                #widget.WindowName(),
                widget.TaskList(),
                #widget.Chord(
                #    chords_colors={
                #        "launch": ("#ff0000", "#ffffff"),
                #    },
                #    name_transform=lambda name: name.upper(),
                #),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                #widget.Systray(),  # X11
                widget.StatusNotifier(), # python-dbus-next, python-pyxdg ?
                #widget.Clipboard(), # no clip manager
                widget.Sep(padding=8, linewidth=0),
                widget.TextBox("V"),
                widget.PulseVolume(volume_app="pavuvontrol"),  # unused option?
                widget.Sep(padding=8, linewidth=0),
                widget.TextBox("L"),
                widget.Backlight(backlight_name="amdgpu_bl0", change_command="brightnessctl s {0}%", step=5),
                widget.Sep(padding=8, linewidth=0),
                widget.TextBox("B"),
                widget.Battery(),
                widget.Sep(padding=8, linewidth=0),
                widget.TextBox("T"),
                widget.ThermalSensor(tag_sensor="Tctl"),
                widget.Sep(padding=8, linewidth=0),
                widget.Clock(format="%Y-%m-%d %H:%M (%a, KW %W, %b)"),
                #widget.QuickExit(),
                #widget.Wallpaper(directory="~/Pictures/Screensaver/", random_selection=True, wallpaper_command=["swaybg", "-i"])
            ],
            #24,
            26,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
    ),
]

# needs feh, does not work on wayland
#widget.Wallpaper(directory="~/Pictures/Wallpaper/", random_selection=True)

# TODO add xembed tray on x11
#if qtile.core.name == "x11":
#    screens[0].bottom.append(widget.Systray())



# Drag floating layouts
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    #Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    # use win ctrl click to resize (libinput problem with touchpad, two finger click detection
    Drag([mod, "control"], "Button1", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    #Click([mod], "Button2", lazy.window.bring_to_front()),
    # raise window on left click
    Click([mod], "Button1", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
        Match(title="SeamLess"),  # launcher
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
#wl_input_rules = None

# TODO configure also for x11

from libqtile.backend.wayland import InputConfig
wl_input_rules = {
    "*": InputConfig(tap=True, tap_button_map="lrm", click_method="clickfinger"),
    #"type:keyboard": InputConfig(kb_layout="de", kb_variant="nodeadkeys", kb_options="caps:escape"),
    #"type:keyboard": InputConfig(kb_layout="us", kb_variant="intl", kb_options="caps:escape"),
    "type:keyboard": InputConfig(kb_layout="usmod", kb_options="caps:escape"),
}

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


