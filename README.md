
# Qtile Configuration

A snapshot of the Qtile config I'm currently working on.

This includes a DBus Service, which allows creating groups, moving windows, etc used by my work-in-progress application launcher:

[SeamLess](https://gitlab.gnome.org/phragment/seamless)


Shout-out to:

- [Qtile](http://www.qtile.org/)



