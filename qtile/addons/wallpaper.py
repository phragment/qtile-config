
import os
import random


class Wallpapers:

    def __init__(self, log, qtile):
        self.log = log.getChild("Wallpapers")
        self.qtile = qtile

        self.dp_dirs = os.path.expanduser("~/Pictures/wallpaper")

        self.dp_cur = ""
        self.dp_index = 0

        self.dp_lockscreen = os.path.expanduser("~/Pictures/lockscreen")

        self.wallpapers = {}

        # seed with system time
        random.seed()

        self.switch(init=True)

    def switch(self, init=False):
        self.log.info("switch wallpaper source")
        if not init:
            self.dp_index += 1
        dirs = self.get_dirs(self.dp_dirs)
        self.dp_wallpapers = dirs[self.dp_index % len(dirs)]
        self.log.info("new wallpaper dir %s", self.dp_wallpapers)

        self.roll()

    def roll(self):
        self.log.info("reroll wallpapers")

        self.candidates = self.get_random(self.dp_wallpapers)

        self.wallpapers.clear()

        for group in self.qtile.groups:
            self.wallpapers[group.label] = self.candidates.pop(0)

        self.update(force=True)

    def update(self, force=False):
        self.log.info("update wallpaper")

        for screen in self.qtile.screens:
            gname = screen.group.name

            if screen.group != screen.previous_group or force:
                self.log.info("update screen %s to group %s", screen.index, screen.group.label)

                wp = self.wallpapers.get(screen.group.label, "")
                if not wp:
                    continue

                screen.cmd_set_wallpaper(wp)

    def get_lockscreen(self):
        return self.get_random(self.dp_lockscreen)[0]

    def add_group(self, label):
        self.log.info("add group %s", label)
        self.wallpapers[label] = self.candidates.pop(0)
        #self.update()  # TODO test defer

    def remove_group(self, label):
        self.log.info("remove group %s", label)
        wp = self.wallpapers[label]
        self.candidates.append(wp)
        del self.wallpapers[label]
        self.update()

    def get_dirs(self, dp):
        dirs = []
        for de in os.scandir(dp):
            if de.is_dir():
                dirs.append(de.path)
        dirs = sorted(dirs)
        return dirs

    def get_files(self, dp):
        files = []
        for de in os.scandir(dp):
            if de.is_file():
                files.append(de.path)
        #files = sorted(files)
        return files

    def get_random(self, dp):
        files = self.get_files(dp)

        # load all filenames and randomize list
        random.shuffle(files)
        fps = [os.path.join(dp, fn) for fn in files]

        return fps

