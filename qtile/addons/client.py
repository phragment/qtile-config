#!/usr/bin/env python3

import asyncio

# python-dbus-next
import dbus_next


# TODO share this code with launcher?
class Client:

    def __init__(self):
        self.service_name = "org.qtile.inofficial"
        self.path = "/org/qtile"
        self.loop = asyncio.new_event_loop()

    def connect(self):
        self.loop.run_until_complete(self._connect())

    async def _connect(self):
        self.bus = await dbus_next.aio.MessageBus().connect()

        introspection = await self.bus.introspect(
            self.service_name,
            self.path
        )

        self.proxy = self.bus.get_proxy_object(
            self.service_name,
            self.path,
            introspection
        )

    def disconnect(self):
        self.loop.run_until_complete(self._disconnect())

    async def _disconnect(self):
        await self.bus.wait_for_disconnect()

    def list_screens(self):
        self.loop.run_until_complete(self._list_screens())

    async def _list_screens(self):
        iface = self.proxy.get_interface("org.qtile")
        ret = await iface.call_list_screens()
        print(ret)

    def list_groups(self):
        self.loop.run_until_complete(self._list_groups())

    async def _list_groups(self):
        iface = self.proxy.get_interface("org.qtile")
        ret = await iface.call_list_groups()
        print(ret)

    def list_layouts(self):
        self.loop.run_until_complete(self._list_layouts())

    async def _list_layouts(self):
        iface = self.proxy.get_interface("org.qtile")
        ret = await iface.call_list_layouts()
        print(ret)

    def list_windows(self):
        self.loop.run_until_complete(self._list_windows())

    async def _list_windows(self):
        iface = self.proxy.get_interface("org.qtile.group")
        ret = await iface.call_list_windows("")
        print(ret)

if __name__ == "__main__":

    client = Client()
    client.connect()

    client.list_screens()
    client.list_groups()
    client.list_layouts()
    client.list_windows()

    # hangs?
    #client.disconnect()





