
# python-dbus-next
#import dbus_next


class DBusService2:

    def __init__(self, log, qtile):
        self.log = log.getchild("DBusService")
        self.qtile = qtile

    # TODO properties qtile_version api_version




# python-dbus
import dbus
import dbus.service
from dbus.mainloop.glib import DBusGMainLoop

from gi.repository import GLib

import threading


class DBusService(dbus.service.Object):

    __version__ = "dev"

    def __init__(self, log, qtile):
        self.log = log.getChild("DBusService")
        self.qtile = qtile

        bus = dbus.SessionBus(mainloop=DBusGMainLoop())
        bus_name = dbus.service.BusName("org.qtile.inofficial", bus)
        try:
            dbus.service.Object.__init__(self, bus_name, "/org/qtile")
        except KeyError:
            self.log.error("could not connect to dbus")
        self.fred = None

    def start(self):
        if self.fred:
            return
        self.log.info("start")
        self.fred = threading.Thread(target=self.routine)
        self.fred.start()

    def stop(self):
        self.log.info("stop")
        self.loop.quit()
        self.fred.join()

    def routine(self):
        self.log.info("starting loop")
        self.loop = GLib.MainLoop()
        self.loop.run()
        #self.remove_from_connection()

    @dbus.service.method("org.qtile", in_signature=None, out_signature="s")
    def get_api_version(self):
        self.log.info("get api version")
        return self.__version__

    @dbus.service.method("org.qtile", in_signature=None, out_signature="ai")
    def list_screens(self):
        self.log.info("get screens")

        # FIXME qtile enhancement, how to get output name from screen?
        # from libqtile.backend.wayland.Output ???

        screens = []
        for screen in self.qtile.screens:
            self.log.info("%s", screen)
            screens.append(screen.index)
        return screens

    @dbus.service.method("org.qtile", in_signature=None, out_signature="as")
    def list_groups(self):
        self.log.info("get groups")
        groupnames = list(self.qtile.groups_map.keys())
        return groupnames

    @dbus.service.signal("org.qtile", signature="i")
    def new_window(self, pid):
        self.log.info("new window %s", pid)

    @dbus.service.method("org.qtile.group", in_signature="s", out_signature="ai")
    def list_windows(self, group):
        self.log.info("list windows %s", group)
        if group:
            gobj = self.qtile.groups_map[group]
        else:
            gobj = self.qtile.current_group
        pids = []
        for window in gobj.windows:
            pids.append(window.get_pid())
            self.log.info("pid %s", window.get_pid())
        return pids

    @dbus.service.method("org.qtile.group", in_signature="s", out_signature="")
    def new(self, group):
        self.log.info("new group %s", group)
        self.qtile.add_group(group)
        #self.qtile.groups_map[name].toscreen()
        gobj = self.qtile.groups_map[group]
        self.qtile.current_screen.set_group(gobj)
        self.log.info("new group should be available")

    #@dbus.service.method("org.qtile.layout", in_signature="", out_signature="as")
    #def list(self):
    @dbus.service.method("org.qtile", in_signature="", out_signature="as")
    def list_layouts(self):
        self.log.info("list layouts")
        layouts = []
        for layout in self.qtile.config.layouts:
            layouts.append(layout.name)
        return layouts


    # TODO optional group?
    @dbus.service.method("org.qtile.layout", in_signature="s", out_signature="")
    def set(self, layout):
        self.log.info("set layout %s", layout)
        gobj = self.qtile.current_group
        gobj.layout = layout

    # TODO optional group?
    @dbus.service.method("org.qtile.layout", in_signature="", out_signature="s")
    def get(self):
        self.log.info("set layout %s", layout)
        gobj = self.qtile.current_group
        return str(gobj.layout)



    # window.togroup
    # window.toscreen


    @dbus.service.method("org.qtile.window", in_signature="i", out_signature="")
    def move_left(self, pid):
        self.log.info("move window %s left", pid)

        #self.log.info("map %s", qtile.windows_map)
        # get window from pid
        #windows = [w for w in self.qtile.windows_map.values() if isinstance(w, base.Window)]
        windows = [w for w in self.qtile.windows_map.values() if isinstance(w, backend.base.Window)]
        #self.log.info("windows %s", windows)

        window = None
        for w in windows:
            #self.log.info("window pid %s", w.get_pid())
            if w.get_pid() == pid:
                window = w
                break
        if not window:
            return

        self.log.info("moving window")
        #.cmd_shuffle_left()
        gobj = self.qtile.current_group
        layout = gobj.layout
        self.log.info("layout %s", layout)
        #self.log.info("layout dir %s", dir(layout))
        layout.cmd_swap_left()

    # TODO unify most code with move_left
    @dbus.service.method("org.qtile.window", in_signature="i", out_signature="")
    def move_right(self, pid):
        self.log.info("move window %s right", pid)

        #self.log.info("map %s", qtile.windows_map)
        # get window from pid
        #windows = [w for w in self.qtile.windows_map.values() if isinstance(w, base.Window)]
        windows = [w for w in self.qtile.windows_map.values() if isinstance(w, backend.base.Window)]
        #self.log.info("windows %s", windows)

        window = None
        for w in windows:
            #self.log.info("window pid %s", w.get_pid())
            if w.get_pid() == pid:
                window = w
                break
        if not window:
            return

        self.log.info("moving window")
        #.cmd_shuffle_right() 
        gobj = self.qtile.current_group
        layout = gobj.layout
        self.log.info("layout %s", layout)
        layout.cmd_swap_right()

    @dbus.service.method("org.qtile.window", in_signature="iiiii", out_signature="")
    def move_float(self, pid, x, y, w, h):
        self.log.info("move window, pid %s, x %s, y %s, w %s, h %s", pid, x, y, w, h)
        # get window from pid
        windows = [w for w in self.qtile.windows_map.values() if isinstance(w, backend.base.Window)]

        # TODO set floating

        window = None
        for w_ in windows:
            #self.log.info("window pid %s", w.get_pid())
            if w_.get_pid() == pid:
                window = w_
                break
        if not window:
            return

        if x >= 0 and y >= 0:
            self.log.info("moving window")
            window.cmd_set_position_floating(x, y)
        if w > 0 and h > 0:
            self.log.info("resize window")
            window.cmd_set_size_floating(w, h)


    @dbus.service.method("org.qtile.group", in_signature="ss", out_signature="")
    def set_wallpaper(self, wp, group):
        pass  # TODO
        # screen.cmd_set_wallpaper(wp)















































